import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Panel extends JPanel {
	
	JButton searchButton;
	
	JLabel label1_Value1;
	JLabel label1_Value2;
	
	JLabel label2_Value1;
	JLabel label2_Value2;
	JLabel label2_Value3;
	JLabel label2_Value4;
	
	

	JLabel label3_comments;
	
	
	JLabel label5_Value1;
	JLabel label5_Value2;
	JLabel label5_Value3;
	JLabel label5_Value4;
	JLabel label5_Value5;
	
	
	JLabel label7_Value1;
	JLabel label7_Value2;
	JLabel label7_Value3;
	
	
	JLabel label8_Value1;
	JLabel label8_Value2;
	
	
	

	

	

	


	public Panel() {
		setSize(300, 300);

		prepareUIComponents();
	}

	private void prepareUIComponents() {

		setLayout(new GridBagLayout());

		GridBagConstraints gridBagConstraints;
		
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		
		
		add(addSearchPanel(), gridBagConstraints);
		
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);

		add(addForm(), gridBagConstraints);

	}

	private Component addSearchPanel() {
		
		JPanel form = new JPanel();
		form.setAlignmentX(LEFT_ALIGNMENT);
		
		form.add(new JTextField(16));
		searchButton = new JButton("Search");
		form.add(searchButton);
		
		return form;
	}

	private Component addForm() {
		JPanel form = new JPanel();
		form.setLayout(new GridBagLayout());

		GridBagConstraints gridBagConstraints;
		JPanel panel1;

		// add Label 1
		panel1 = new JPanel();
		panel1.setLayout(new GridBagLayout());
		panel1.setBorder(BorderFactory.createTitledBorder("Label One"));
		panel1.setMaximumSize(new Dimension(200, 150));
		panel1.setMinimumSize(new Dimension(200, 150));
		panel1.setPreferredSize(new Dimension(200, 150));
		
		
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        label1_Value1 = new JLabel("");
		panel1.add(label1_Value1,gridBagConstraints);
		
		
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        label1_Value2 = new JLabel("");
		panel1.add(label1_Value2,gridBagConstraints);
		
		

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		form.add(panel1, gridBagConstraints);

		// add Label 2
		panel1 = new JPanel();
		panel1.setLayout(new GridBagLayout());
		panel1.setBorder(BorderFactory.createTitledBorder("Label Two"));
		panel1.setMaximumSize(new Dimension(200, 150));
		panel1.setMinimumSize(new Dimension(200, 150));
		panel1.setPreferredSize(new Dimension(200, 150));
		
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        label2_Value1 = new JLabel("");
		panel1.add(label2_Value1,gridBagConstraints);
		
		
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        label2_Value2 = new JLabel("");
		panel1.add(label2_Value2,gridBagConstraints);
		
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        label2_Value3 = new JLabel("");
		panel1.add(label2_Value3,gridBagConstraints);
		
		
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        label2_Value4 = new JLabel("");
		panel1.add(label2_Value4,gridBagConstraints);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		form.add(panel1, gridBagConstraints);

		// add Label 3 ie comments
		panel1 = new JPanel();
		panel1.setLayout(new GridBagLayout());
		panel1.setBorder(BorderFactory.createTitledBorder("Comments"));
		panel1.setMaximumSize(new Dimension(200, 150));
		panel1.setMinimumSize(new Dimension(200, 150));
		panel1.setPreferredSize(new Dimension(200, 150));
		
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        label3_comments = new JLabel("");
		panel1.add(label3_comments,gridBagConstraints);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		form.add(panel1, gridBagConstraints);

		// add Label 4 ie Graph
		panel1 = new JPanel();
		panel1.setLayout(new GridBagLayout());
		panel1.setBorder(BorderFactory.createTitledBorder(""));
		panel1.setMaximumSize(new Dimension(200, 150));
		panel1.setMinimumSize(new Dimension(200, 150));
		panel1.setPreferredSize(new Dimension(200, 150));

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		form.add(panel1, gridBagConstraints);

		// add Label 5
		panel1 = new JPanel();
		panel1.setLayout(new GridBagLayout());
		panel1.setBorder(BorderFactory.createTitledBorder("Label Five"));
		panel1.setMaximumSize(new Dimension(200, 150));
		panel1.setMinimumSize(new Dimension(200, 150));
		panel1.setPreferredSize(new Dimension(200, 150));
		
		
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        label5_Value1 = new JLabel("");
		panel1.add(label5_Value1,gridBagConstraints);
		
		
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        label5_Value2 = new JLabel("");
		panel1.add(label5_Value2,gridBagConstraints);
		
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        label5_Value3 = new JLabel("");
		panel1.add(label5_Value3,gridBagConstraints);
		
		
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        label5_Value4 = new JLabel("");
		panel1.add(label5_Value4,gridBagConstraints);
		
		
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        label5_Value5 = new JLabel("");
		panel1.add(label5_Value5,gridBagConstraints);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		form.add(panel1, gridBagConstraints);

		// add Label 6
		panel1 = new JPanel();
		panel1.setLayout(new GridBagLayout());
		panel1.setBorder(BorderFactory.createTitledBorder("Label Six"));
		panel1.setMaximumSize(new Dimension(200, 150));
		panel1.setMinimumSize(new Dimension(200, 150));
		panel1.setPreferredSize(new Dimension(200, 150));

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		form.add(panel1, gridBagConstraints);

		// add Label 7
		panel1 = new JPanel();
		panel1.setLayout(new GridBagLayout());
		panel1.setBorder(BorderFactory.createTitledBorder("Label Seven"));
		panel1.setMaximumSize(new Dimension(200, 150));
		panel1.setMinimumSize(new Dimension(200, 150));
		panel1.setPreferredSize(new Dimension(200, 150));
		
		
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        label7_Value1 = new JLabel("");
		panel1.add(label7_Value1,gridBagConstraints);
		
		
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        label7_Value2 = new JLabel("");
		panel1.add(label7_Value2,gridBagConstraints);
		
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        label7_Value3 = new JLabel("");
		panel1.add(label7_Value3,gridBagConstraints);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		form.add(panel1, gridBagConstraints);

		// add Label 8
		panel1 = new JPanel();
		panel1.setLayout(new GridBagLayout());
		panel1.setBorder(BorderFactory.createTitledBorder("Label Eight"));
		panel1.setMaximumSize(new Dimension(200, 150));
		panel1.setMinimumSize(new Dimension(200, 150));
		panel1.setPreferredSize(new Dimension(200, 150));
		
		
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        label8_Value1 = new JLabel("");
		panel1.add(label8_Value1,gridBagConstraints);
		
		
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        label8_Value2 = new JLabel("");
		panel1.add(label8_Value2,gridBagConstraints);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		form.add(panel1, gridBagConstraints);

		// add Label 9
		panel1 = new JPanel();
		panel1.setLayout(new GridBagLayout());
		panel1.setBorder(BorderFactory.createTitledBorder("Label Nine"));
		panel1.setMaximumSize(new Dimension(200, 150));
		panel1.setMinimumSize(new Dimension(200, 150));
		panel1.setPreferredSize(new Dimension(200, 150));

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		form.add(panel1, gridBagConstraints);

		return form;
	}
	
	
	public JButton getSearchButton() {
		return searchButton;
	}
	
	public JLabel getLabel1_Value1() {
		return label1_Value1;
	}

	public JLabel getLabel1_Value2() {
		return label1_Value2;
	}
	
	
	public JLabel getLabel3_comments() {
		return label3_comments;
	}
	
	
	public JLabel getLabel5_Value1() {
		return label5_Value1;
	}

	public JLabel getLabel5_Value2() {
		return label5_Value2;
	}

	public JLabel getLabel5_Value3() {
		return label5_Value3;
	}

	public JLabel getLabel5_Value4() {
		return label5_Value4;
	}

	public JLabel getLabel5_Value5() {
		return label5_Value5;
	}
	
	
	public JLabel getLabel7_Value1() {
		return label7_Value1;
	}

	public JLabel getLabel7_Value2() {
		return label7_Value2;
	}

	public JLabel getLabel7_Value3() {
		return label7_Value3;
	}
	

	public JLabel getLabel8_Value1() {
		return label8_Value1;
	}

	public JLabel getLabel8_Value2() {
		return label8_Value2;
	}
	
	
	public JLabel getLabel2_Value1() {
		return label2_Value1;
	}

	public JLabel getLabel2_Value2() {
		return label2_Value2;
	}

	public JLabel getLabel2_Value3() {
		return label2_Value3;
	}

	public JLabel getLabel2_Value4() {
		return label2_Value4;
	}

}
