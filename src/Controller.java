import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import javax.swing.JDialog;

public class Controller {

	private Panel panel;

	public Controller() {
		panel = new Panel();
		addListeners();
	}

	// add listener on search button.
	//Onclick -> load properties file (data.properties) and pass these values in various labels
	
	private void addListeners() {

		panel.getSearchButton().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				try {
					Properties p =loadProperties();
					populateData(p);
				} catch (IOException e1) {
					e1.printStackTrace();
				}

			}

		});

	}

	private Properties loadProperties() throws IOException {

		Properties props = new Properties();
		FileInputStream fis = new FileInputStream("c:/data.properties");

		// loading properites from properties file
		props.load(fis);

		
		return props;
	}

	private void populateData(Properties props) {
		
		// reading proeprty
		
		panel.getLabel1_Value1().setText(props.getProperty("label1.value1"));
		panel.getLabel1_Value2().setText(props.getProperty("label1.value2"));
		
		panel.getLabel2_Value1().setText(props.getProperty("label2.value1"));
		panel.getLabel2_Value2().setText(props.getProperty("label2.value2"));
		panel.getLabel2_Value3().setText(props.getProperty("label2.value3"));
		panel.getLabel2_Value4().setText(props.getProperty("label2.value4"));
		
		
		panel.getLabel3_comments().setText(props.getProperty("label3.comments"));
		
		
		panel.getLabel5_Value1().setText(props.getProperty("label5.value1"));
		panel.getLabel5_Value2().setText(props.getProperty("label5.value2"));
		panel.getLabel5_Value3().setText(props.getProperty("label5.value3"));
		panel.getLabel5_Value4().setText(props.getProperty("label5.value4"));
		panel.getLabel5_Value5().setText(props.getProperty("label5.value5"));
		
		
		panel.getLabel7_Value1().setText(props.getProperty("label7.value1"));
		panel.getLabel7_Value2().setText(props.getProperty("label7.value2"));
		panel.getLabel7_Value3().setText(props.getProperty("label7.value3"));
		
		panel.getLabel8_Value1().setText(props.getProperty("label8.value1"));
		panel.getLabel8_Value2().setText(props.getProperty("label8.value2"));

	}

	public void showDialog() {
		JDialog dialog = new JDialog();
		dialog.setTitle("Automation Anywhere");
		dialog.setContentPane(panel);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.pack();
		dialog.show();

	}

}
